'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
var db = require('./db')
var express = require('express')
// const socketio = require('socket.io');
// const socketEvents = require('./web/socket'); 
var logger = require('morgan');
var bodyParser = require('body-parser');
var path = require('path');
var chat = require('./routes/chat');
var mongoose = require('mongoose'),
cors = require('cors');

//recently added
// var app = express();
// var server = app.listen(3000);
// var io = require('socket.io').listen(server);

//--------------------
// mongoose.Promise = global.Promise;

module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};
// app.use(express.static('public'));
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization, Origin, Host, Accept, Origin, Referer, User-Agent');
  next();
});
app.use(cors)

//change here in case of error
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, 'dist')));
app.use('/chat', chat);

//------------------------------------------------------

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }
  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  }
});

