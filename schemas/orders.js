var mongoose = require('mongoose')

module.exports = mongoose.model('orders',{
    cart: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'userCart'
    },
    userId: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'users'
    },
})